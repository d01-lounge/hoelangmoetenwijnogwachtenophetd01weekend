import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import Countdown from './components/Countdown';


export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      date: 'a',
      title: 'b'
    }

    this.fetchConfigFile()
  }

  fetchConfigFile = () => {
    axios.get('http://localhost:3019/data')
      .then((response) => {
        this.setState({
          date: response.data.date,
          title: response.data.what
        });
      })
  }

  render() {
    return (
      <div className="App">

        <header className="App-header">
          <h1 className="App-title">{this.state.title}</h1>
        </header>

        <article className="App-content">
          <Countdown date={this.state.date} />
        </article>

        <footer className="App-footer">
          Hyperion &copy; { new Date().getFullYear() }
        </footer>

      </div>
    );
  }
}